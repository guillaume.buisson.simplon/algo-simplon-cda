/*
Écrire un algorithme qui permette de connaître ses chances de gagner au tiercé,
quarté, quinté et autres impôts volontaires.
On demande à l’utilisateur le nombre de chevaux partants, et le nombre de chevaux joués.
Les deux messages affichés devront être :
Dans l’ordre : une chance sur X de gagner Dans le désordre : une chance sur Y de gagner
X et Y nous sont donnés par la formule suivante,
si n est le nombre de chevaux partants et p le nombre de chevaux joués
(on rappelle que le signe ! signifie "factorielle") :
X = n ! / (n - p) ! Y = n ! / (p ! * (n – p) !)
*/


let prompt = require('prompt');

prompt.start();

prompt.get(['number'], function (err, result) {

});