/*
Ecrire un algorithme qui affiche la moyenne d’une suite d’entiers
*/

let prompt = require('prompt');
let Numbers = [];

prompt.start();

for (let i = 1; i <= 5; i++) {
    Numbers.push('Number' + i);
}

prompt.get(Numbers, function (err, result) {
    moyenne(result);
});

function moyenne(result) {
    let allResult = [], totalResult = 0;
    for (let i = 1; i <= 5; i++) {
        let numberIndex = 'Number' + i;
        allResult.push(result[numberIndex])
    }
    for (let i = 0; i < allResult.length; i++) {
        totalResult = totalResult + parseInt(allResult[i]);
    }
    console.log(totalResult / 5);
}
