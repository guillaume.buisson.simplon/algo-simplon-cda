/*
Ecrire une action qui fournit les félicitations ou l’ajournement d’un élève suivant sa note
en utilisant Si-alors-sinon.
*/

let prompt = require('prompt');

prompt.start();

prompt.get(['note'], function (err, result) {
    goodOrBad(result);
});

function goodOrBad(note) {
    let myNote = note['note'];
    if(myNote >= 15) {
        console.log('WaoW');
    } else if (myNote >= 10 && myNote < 15) {
        console.log('tu as réussis');
    }
    else {
        console.log('tu as échoué');
    }
}