/*
Ecrire un algorithme qui demande successivement 20 nombres à l’utilisateur,
et qui lui dise ensuite quel était le plus grand parmi ces 20 nombres :
Modifiez ensuite l’algorithme pour que le programme affiche de surcroît en quelle position avait été saisie ce nombre :
C’était le nombre numéro 2
*/

let prompt = require('prompt');
let Numbers = [];

prompt.start();

for (let i = 1; i <= 20; i++) {
    Numbers.push('Number' + i);
}

prompt.get(Numbers, function (err, result) {
    WhoIsTheBigInt(result);
});

function WhoIsTheBigInt(result) {
    let bigInt = 0, index = '';
    for (let i = 0; i < 20; i++) {
        let numberIndex = 'Number' + i;
        if(result[numberIndex] >= bigInt) {
            bigInt = result[numberIndex];
            index = numberIndex;
        }
    }
    console.log(index + ' : ' + bigInt);
}
