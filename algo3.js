/*
Ecrire un algorithme qui demande un nombre de départ, et qui calcule sa factorielle.
NB : la factorielle de 8, notée 8 !, vaut
1 x 2 x 3 x 4 x 5 x 6 x 7 x 8
*/


let prompt = require('prompt');

prompt.start();

prompt.get(['number'], function (err, result) {
    factorielle(result);
});

function factorielle(result) {
    let number = result['number'];
    let factorielle = 1;
    for (let i = 1; i <= number; i++) {
            factorielle = factorielle * i;
    }
    console.log(factorielle);
}